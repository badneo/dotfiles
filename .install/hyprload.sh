if [ -f "$HOME/.local/share/hyprload/hyprload.sh" ]; 
then

# if file exist
    echo "Hyprload is already installed"
    break
else

# file does not exist then install
    echo "Installing Hyprload"
    curl -sSL https://raw.githubusercontent.com/Duckonaut/hyprload/main/install.sh | bash
fi